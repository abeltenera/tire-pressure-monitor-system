﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TirePressureMonitor
{
    public interface ISensor
    {
        double PopNextPressurePsiValue();        
    }
}
