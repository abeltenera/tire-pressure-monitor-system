﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TirePressureMonitor.Exceptions;

namespace TirePressureMonitor
{
    public class Alarm
    {

        private readonly double _lowPressureThreshold = 17;
        private readonly double _highPressureThreshold = 21;

        readonly ISensor _sensor;

        bool _alarmOn = false;

        public Alarm(double lowPressureThreshold = 17, double highPressureThreshold = 21)
            : this(new Sensor(), lowPressureThreshold, highPressureThreshold)
        { }

        public Alarm(ISensor sensor, double lowPressureThreshold = 17, double highPressureThreshold = 21)
        {
            _sensor = sensor;
            if (lowPressureThreshold < 0 || highPressureThreshold < 0 || lowPressureThreshold > highPressureThreshold)
            {
                throw new ArgumentOutOfRangeException("Invalid threshold values");
            }
            _lowPressureThreshold = lowPressureThreshold;
            _highPressureThreshold = highPressureThreshold;
        }


        /// <summary>
        /// Checks the pressure from sensor and sets alarm
        /// </summary>
        public void Check()
        {
            double psiPressureValue = _sensor.PopNextPressurePsiValue();

            if (psiPressureValue < 0)
                throw new SensorInvalidPressureException(String.Format("Sensor output a negative value of {0} psi", psiPressureValue));

            _alarmOn = psiPressureValue < _lowPressureThreshold || _highPressureThreshold < psiPressureValue;

        }

        public bool AlarmOn
        {
            get { return _alarmOn; }
        }
    }
}
