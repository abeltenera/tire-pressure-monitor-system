﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TirePressureMonitor.Exceptions
{
    public class SensorInvalidPressureException : Exception
    {
        public SensorInvalidPressureException(string message) : base(message) { }
    }
}
