# Tire Pressure Exercise

## Overview
The whole solution consists on three projects. 
- TirePressureMonitor: Library Project that contains the Alarm and Sensor class
- TirePressureMonitorTests: Unit testing project that contains different test cases for Alarm.cs
- TirePressureMonitorClients: Contains example clients just to make sure the code changes won't affect them

## Why the original code was not testable?
The original Alarm class depended directly on the Sensor class, that would be used to pop PSI values and evaluate them.
This direct dependency made Alarm class not testable, because we are not able to create test cases for specific values, which violates
the Depedency Inversion Principle of SOLID.


## Changes made
First, it was created the ISensor interface, which contains the signature for the PSI value popping method. Then, the Alarm class
was modified to rely on ISensor instead of Sensor to check pressure, and Sensor class was changed to become an implementation of this interface. 
In the unit test project, another implementation of ISensor was created, MockSensor, to inject specific values in Alarm class and test different scenarios.
Also, the pressure thresholds were made configurable.

## Other considerations
To fully follow the SOLID principles, more changes should be made, but they would impact the existing clients.
The Alarm class currently violates the Single Resposibility principal, because it should only be responsible for raising an alarm in a given condition, and not be responsible for checking on pressure.
If the client impact was not a problem, it would make sense to create an abstract class with some Alarm behaviour (AAlarm), with two methods:

public void Check(double val)

protected virtual void Validate(double val)

protected virtual bool Evaluate(double val)

The Check method would invoke Validate first, to validate input value, and Evaluate, to determine if the alarm is to be raised or not. Each implementation of AAlarm, would be able to override Validate and Evaluate to serve 
different scenarios. Then, a controller class would be created to control instances of AAlarm and ISensor. This controller would also be able to be instanced with any implementation of
AAlarm and ISensor.
