﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TirePressureMonitor;

namespace TirePressureMonitorClients
{
    public class AnAlarmClient3
    {
        // A class with the only goal of simulating a dependency on Alert
        // that has impact on the refactoring.

        private Alarm _anAlarm;

        public AnAlarmClient3()
        {
            _anAlarm = new Alarm();
        }

        public void DoSomething()
        {
            _anAlarm.Check();
        }

        public void DoSomethingElse()
        {
            bool isAlarmOn = _anAlarm.AlarmOn;
        }
    }
}
