﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TirePressureMonitorClients
{
    //This program just executes all the clients
    class Program
    {
        static void Main(string[] args)
        {
            ASensorClient aSensorClient = new ASensorClient();

            AnAlarmClient1 anAlarmClient1 = new AnAlarmClient1();
            AnAlarmClient2 anAlarmClient2 = new AnAlarmClient2();
            AnAlarmClient3 anAlarmClient3 = new AnAlarmClient3();
            anAlarmClient3.DoSomething();
            anAlarmClient3.DoSomethingElse();


        }
    }
}
