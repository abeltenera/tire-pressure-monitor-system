﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TirePressureMonitor;

namespace TirePressureMonitorTests
{

    /// <summary>
    /// MockSensor mocks the sensor to test Alarm class behaviour with some given values
    /// </summary>
    public class MockSensor : ISensor
    {
        private readonly double _value;

        public MockSensor(double value)
        {
            _value = value;
        }

        public double PopNextPressurePsiValue()
        {
            return _value;
        }
    }
}
