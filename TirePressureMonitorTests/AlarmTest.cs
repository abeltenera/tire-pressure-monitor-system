﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TirePressureMonitor;
using TirePressureMonitor.Exceptions;

namespace TirePressureMonitorTests
{
    [TestClass]
    public class AlarmTests
    {
        /// <summary>
        /// The alarm check should raise an error on negative values
        /// </summary>
        [TestMethod]
        public void Check_Pressure_With_Invalid_Amount()
        {
            double lowPressureThreshold = 17;
            double highPressureThreshold = 21;
            MockSensor sensor = new MockSensor(-1);
            bool invalid = false;
            try
            {
                //Checks if a negative pressure throws an error
                Alarm alarm = new Alarm(sensor, lowPressureThreshold, highPressureThreshold);
                alarm.Check();
            }
            catch (SensorInvalidPressureException)
            {
                invalid = true;
            }
            Assert.AreEqual(true, invalid);
        }

        /// <summary>
        /// Alarm must be on, on high pressure
        /// </summary>
        [TestMethod]
        public void Check_Alarm_High_Pressure_On()
        {
            double lowPressureThreshold = 17;
            double highPressureThreshold = 21;

            // Sets a mock sensor with a value higher than the threshold
            MockSensor sensor = new MockSensor(24);

            //Checks the pressure
            Alarm alarm = new Alarm(sensor, lowPressureThreshold, highPressureThreshold);
            alarm.Check();
            bool alarmOn = alarm.AlarmOn;

            Assert.AreEqual(true, alarmOn);
        }

        /// <summary>
        /// Alarm must be on, on low pressure
        /// </summary>
        [TestMethod]
        public void Check_Alarm_Low_Pressure_On()
        {
            double lowPressureThreshold = 17;
            double highPressureThreshold = 21;

            // Sets a mock sensor with a value higher than the threshold
            MockSensor sensor = new MockSensor(11);

            //Checks the pressure
            Alarm alarm = new Alarm(sensor, lowPressureThreshold, highPressureThreshold);
            alarm.Check();
            bool alarmOn = alarm.AlarmOn;

            Assert.AreEqual(true, alarmOn);
        }

        /// <summary>
        /// Alarm must be off, on pressure between the threshold params
        /// </summary>
        [TestMethod]
        public void Check_Alarm_Off()
        {
            double lowPressureThreshold = 17;
            double highPressureThreshold = 21;

            // Sets a mock sensor with a value higher than the threshold
            MockSensor sensor = new MockSensor(19);

            //Checks the pressure
            Alarm alarm = new Alarm(sensor, lowPressureThreshold, highPressureThreshold);
            alarm.Check();
            bool alarmOn = alarm.AlarmOn;

            Assert.AreEqual(false, alarmOn);
        }
 
        /// <summary>
        /// Constructor must throw an error when threshold values are negative
        /// </summary>
        [TestMethod]
        public void Constructor_Error_On_Negative_Threshold()
        {            
            MockSensor sensor = new MockSensor(19);

            bool errorThrown = false;

            try
            {
                double lowPressureThreshold = -17;
                double highPressureThreshold = 21;
                Alarm alarm = new Alarm(sensor, lowPressureThreshold, highPressureThreshold);

                lowPressureThreshold = 1;
                highPressureThreshold = -17;
                alarm = new Alarm(sensor, lowPressureThreshold, highPressureThreshold);

            }
            catch(ArgumentOutOfRangeException ex) {
                errorThrown = true;
            }

            Assert.AreEqual(true, errorThrown);
        }

        /// <summary>
        /// Constructor must throw an error when lp threshold is bigger than hp
        /// </summary>
        [TestMethod]
        public void Constructor_Error_On_LPT_Bigger_Than_HPT()
        {
            double lowPressureThreshold = 17;
            double highPressureThreshold = 1;
            MockSensor sensor = new MockSensor(19);

            bool errorThrown = false;

            try
            {
                Alarm alarm = new Alarm(sensor, lowPressureThreshold, highPressureThreshold);

            }
            catch (ArgumentOutOfRangeException ex)
            {
                errorThrown = true;
            }

            Assert.AreEqual(true, errorThrown);
        }

    }
}
